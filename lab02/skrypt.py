import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import matplotlib.patches as patches
import matplotlib.animation as animation
from matplotlib.path import Path
from scipy.spatial import distance

def add_obstacle(x, y, width, height):
    for i in np.arange(_scene.shape[0]):
        for j in np.arange(_scene.shape[1]):
            if j >= x and j <= x + width and i >= y and i <= y + height:
                _scene[j][i] = -1

def add_position(x,y):
    if x < 0 or x >= x_size or y < 0 or y >= y_size or scene[x][y] == -1:
        raise Exception("Wjechales w przeszkode: (" + str(x) + "," + str(y) + ")")
    global x_pos
    global y_pos
    positions[0].append(x)
    positions[1].append(y)
    scene[x][y] = 1
    x_pos = x
    y_pos = y

def move_x(val):
    move(val, 1, 0)

def move_y(val):
    move(val, 0, 1)

def move(val, x, y):
    for i in np.arange(0, abs(val)):
        if val > 0:
            add_position(x_pos + x, y_pos + y)
        else:
            add_position(x_pos - x, y_pos - y)

def update(i):
    line.set_data([x+0.5 for x in positions[0][0:i]], [y+0.5 for y in positions[1][0:i]])
    return line

def coverage():
    full_size = len(scene.flatten())
    obst_size = np.count_nonzero(scene == -1)
    cleaned_size = np.count_nonzero(scene == 1)
    dirty_size = np.count_nonzero(scene == 0)
    coveraged_size = cleaned_size/(full_size - obst_size) * 100

    print("Koncowe polozenie: (" + str(x_pos) + ", " + str(y_pos)+ ")")
    print("Pelna scena: " + str(full_size))
    print("Przeszkody: " + str(obst_size))
    print("Posprzatane pole: " + str(cleaned_size))
    print("Zasmiecone pole: " + str(dirty_size))
    print("Pokrycie: " + str(round(coveraged_size,2)) + "%")

delta = 1
x_size = 40
y_size = 40
x_pos = 0
y_pos = 25
positions=[[],[]]
x = y = np.arange(0, 40.0, delta)
X, Y = np.meshgrid(x, y)
_scene = X*0
_scene[x_pos][y_pos]= 1 # punkt poczatkowy
add_obstacle(0,0,4,16)
add_obstacle(25,32,15,8)
add_obstacle(24,12,4,12)

add_obstacle(9,38,2,2) # Moja przeszkoda do zobrazowania szczególnego przypadku w rogu

scene = _scene.copy()
add_position(x_pos, y_pos)

def PathFromExample():
    move_x(15)
    move_y(1)
    move_x(-15)
    move_y(1)
    move_x(15)
    move_y(1)
    move_x(-15)
    move_y(1)
    move_x(15)
    move_y(1)
    move_x(-15)
    move_y(1)
    move_x(15)
    move_y(-1)
    move_x(15) # zamien na 10
    move_y(-20)
    move_x(-15)
    move_y(14)
    move_x(-15)
    move_y(-7)

# animacja= True
animacja= False

# Uzupelnic kodem
##############################

class HeadingAndMovement():
    '''
    Contains and modifies information about robot's orientation and heading.
    There are four possible Orientations, as descriped in dictionary in the init method.
                              
                                ^ YU 1
                                |
                        XL 4 ---|---> XR 2
                                |
                                  YD 3 
    Each key value pair contains information about movement in a given direction. 
    Direction attribute informs which MX to choose, where X is Direction value(int).
    Moving forwards use adequate movement M, based on Direction. 
    Moving backwards becomes movement forwards with Direction index changed by 2.
    Each turn Right/Left corresponds to increase/decrease Direction by 1, respectively.
    '''
    def __init__(self, DefaultDirection):
        self.__Orientations = {'M0': self.AxisYMoveUp, 'M1': self.AxisXMoveRight, 
                                'M2': self.AxisYMoveDown, 'M3': self.AxisXMoveLeft}
        
        # Min/Max Indexes must correspond to M0-M3 keys in Orientations dictionary
        self.__MinIndex = 0 
        self.__MaxIndex = 3
        
        if DefaultDirection in range(self.__MinIndex, self.__MaxIndex +1): # is Default correct?
            self.__Direction = DefaultDirection
        else:
            Info = 'Incorrect DefaultDirection('+str(self.__MinIndex)+' to '+str(self.__MaxIndex)+'): '+DefaultDirection
            raise Exception(Info)

    # Four basic movement functions
    def AxisYMoveUp(self):
        move_y(1)
    def AxisXMoveRight(self):
        move_x(1)
    def AxisYMoveDown(self):
        move_y(-1)
    def AxisXMoveLeft(self):
        move_x(-1)

    def TurnRight(self):
        self.__Direction += 1
        if self.__Direction == (self.__MaxIndex + 1): # Direction in range circle: 4+1 => 0 
            self.__Direction = self.__MinIndex

    def TurnLeft(self):
        self.__Direction -= 1 
        if self.__Direction == (self.__MinIndex - 1):  # Direction in range circle: 0-1 => 4 
            self.__Direction = self.__MaxIndex

    def DirectionForBackwards(self):
        # Backwards means opposite to current Direction(+/- 2), but still in range(% 4)
        BackwardsDirection = (self.__Direction + 2) % 4
        return BackwardsDirection

    def MoveForward(self):
        MoveName = "M" + str(self.__Direction) # choose adequate key, based on Direction
        self.__Orientations.get(MoveName, lambda: "Invalid Name")()

    def MoveBackward(self):
        MoveName = "M" + str(self.DirectionForBackwards()) # choose key, based on Direction+2
        self.__Orientations.get(MoveName, lambda: "Invalid Name")()

# Example robot
VacuumCleaner = HeadingAndMovement(0) # 0 for AxisYMoveUp

# START
DirCount = 1 # counter for turns
def SneakySnakeAlgorithm():
    global DirCount
    global VacuumCleaner
    global Oscillation

    # ALGORYTM Z TABLICY:
    try:
        VacuumCleaner.MoveForward()
    except:
        if (DirCount % 2) == 1:
            VacuumCleaner.TurnRight()
            try: # consider special case: scene and obstacle corner / end of the scene
                VacuumCleaner.MoveForward()
            except: # Possibly end of scene - increase feedback counter
                Oscillation += 1
            VacuumCleaner.TurnRight()
        else:
            VacuumCleaner.TurnLeft()
            try:
                VacuumCleaner.MoveForward()
            except: # Possibly end of scene - increase feedback counter
                Oscillation += 1
            VacuumCleaner.TurnLeft()
        DirCount += 1 # Shared between Turn Right and Left
        
#######################
# End of the scene is indicated by threefold movement along one x_pos value. 
# Each run will increase Oscillation, until it breaks the while loop.
Oscillation = 0
while Oscillation < 3:
    CurrentXPos = x_pos # Record x_pos before movement
    SneakySnakeAlgorithm() # make a move
    if CurrentXPos is not x_pos: # If x_pos has been changed, robot hasn't reached the end
        Oscillation = 0

_scene[x_pos][y_pos] = 1 
_scene = np.where(scene == 0, -0.5, _scene)

fig = plt.figure()
ax = fig.add_subplot(111)
line, = ax.plot([], [], linewidth=3)
plt.imshow(_scene.transpose(), cmap=cm.RdYlGn, origin='lower', extent=[0, x_size, 0, y_size])
ax.set_title("‘S’ shape pattern algorithm")
plt.grid(True)
if animacja:
    ani = animation.FuncAnimation(fig, update, interval=1,  save_count=1)
else:
    plt.plot(positions[0], positions[1], linewidth=3)
coverage()

plt.show()


